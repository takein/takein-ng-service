'use strict';
(function(){
    
    // var tiaResource = angular.module('TakeInResource', ['ngResource']);
    var tiaResource = angular.module('TakeInResource', []);
    
    //============
    // Some constants
    tiaResource.constant('apiTokenHeaderName', 'X-API-Token');
    
    
    
    //============
    // The API Service

    // Todos: 
    // + Make it easy to toggle HTTPS
    
    
    tiaResource.provider('TakeInAPI', function TakeInAPIProvider(){
        
        var configured = {
            baseUrl: null,
            apiToken: null
        };

        var prefixEndpoint = function(endpoint, queryStringArgs){
            var url = configured.baseUrl + endpoint;
            if (typeof queryStringArgs === 'object' && queryStringArgs !== null){
                var str = [];
                for(var p in queryStringArgs)
                    if (queryStringArgs.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(queryStringArgs[p]));
                }
                url += '?' + str.join("&");
            }
            return url;
        };
        
        // Method exposed for configuration phase
        this.setBaseUrl = function(newBaseUrl){
            configured.baseUrl = newBaseUrl;
        };
        
        // Method exposed for configuration phase
        this.setApiToken = function(newApiToken){
            configured.apiToken = newApiToken;
        };
        
        this.$get = ['$http', '$q', 'apiTokenHeaderName', function($http, $q, apiTokenHeaderName){
            
            // Not allowed to have no BaseUrl..
            if (configured.baseUrl == null){
                throw "Base URL was not set. During your module's configuration, call TakeInAPIProvider.setBaseUrl('...') to set this.";
            }
            
            // If ApiToken was preset, then set it as a header
            if (configured.apiToken){
                $http.defaults.headers.common[apiTokenHeaderName] = configured.apiToken;
            }
            
            //=================================================
            // Return the actions for this Provider. 
            // Exposes a whole bunch of things that you can interact with from Take In API.
            
            var actions = {
                
                /**
                 * Return the current configuration for TakeInAPI
                 * Generally this should be used for debugging purposes only
                 * 
                 * @return object  JSON response
                 */
                configured: function(){
                    
                    return angular.extend({}, configured);      // read only
                    
                },
                
                /**
                 * Retrieve multiple resources from the endpoint 
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                getAll: function(endpoint, opts){
                    
                    opts = opts || {};
                    return $http.get(prefixEndpoint(endpoint, opts.queryString)).then(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Retrieve a single resource from the endpoint
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param id  ID of the dish to be retrieved
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                get: function(endpoint, opts){
                    
                    return $http.get(prefixEndpoint(endpoint)).then(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Overwrite an entire resource (dependent on server implementation)
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param id  ID of the resource to be overwritten
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                post: function(endpoint, data, opts){
                    
                    if (!(data && typeof data === 'object')){
                        throw 'Bad payload format, data must be object or array';
                    }
                    // Todo: additionally validate the data?
                    // Nah that should be dealt with in the controller
                    
                    return $http.post(prefixEndpoint(endpoint), data).then(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Overwrite an entire resource (dependent on server implementation)
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param id  ID of the resource to be overwritten
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                put: function(endpoint, data, opts){
                    
                    if (!(data && typeof data === 'object')){
                        throw 'Bad payload format, must be object or array';
                    }
                    // Todo: additionally validate the data?
                    
                    return $http.put(prefixEndpoint(endpoint), data).then(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Update specific elements of the resource (dependent on server implementation)
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param id  ID of the resource to be updated
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                patch: function(endpoint, data, opts){
                    
                    if (!(data && typeof data === 'object')){
                        throw 'Bad payload format, must be object or array';
                    }
                    // Todo: additionally validate the data?
                    
                    return $http.patch(prefixEndpoint(endpoint), data).then(function(response){
                        return response.data;
                    }).catch(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Delete this resource (dependent on server implementation;
                 * the implementation may choose to soft delete it)
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param id  ID of the resource to be deleted
                 * @param opts  Object of optional options 
                 * @return object  JSON response
                 */
                delete: function(endpoint, opts){
                    
                    return $http.delete(prefixEndpoint(endpoint)).then(function(response){
                        return response.data;
                    });
                    
                },
                
                /**
                 * Deliver credentials via Basic auth header, to retrieve
                 * an API token
                 * 
                 * @param endpoint  Requested endpoint path
                 * @param username  Username
                 * @param password  Password
                 * @return object  JSON response
                 */
                basic: function(endpoint, username, password){
                    
                    var basicAuthHeaderValue = 'Basic ' + btoa( username + ':' + password );
                    var headers = { 
                        Authorization: basicAuthHeaderValue, 
                        'X-API-Token': null             // For logins, don't send existing API Tokens
                    };
                    return $http.post(prefixEndpoint(endpoint), {}, { headers: headers }).then(function(response){
                        if (response.data.api_token){
                            // Expect an API token; set it as the default from now on
                            configured.apiToken = response.data.api_token;
                            $http.defaults.headers.common[apiTokenHeaderName] = configured.apiToken;
                        }
                        return response.data;
                    });
                    
                },
                
            };
            
            return actions;
        }];
        
    });
    
    
})();