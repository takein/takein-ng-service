'use strict';
(function(){
    
    // var tiaResource = angular.module('TakeInResource', ['ngResource']);
    var tiaResource = angular.module('TakeInResource') || angular.module('TakeInResource', []);
    
    
    //================
    // Contains:
    // Order
    //================
    
    
    //================
    // 1. Order Factory definition
    
    tiaResource.factory('Order', ['$http', '$q', 'TakeInAPI', function($http, $q, TakeInAPI){
        
        function Order(orderData){
            
            this.data = {};              // Order main record
            this.shadow = {};         // Shadow record useful to compare for changes
            
            this.lastRetrieved = null;
            this.lastSaved = null;
            
            if (orderData){
                this.data = orderData;
                this.shadow = angular.extend({}, orderData);
            }
            
        }

        /**
         * ID retrieval helper
         * 
         * @return ID
         */        
        Order.prototype.id = function(){
            
            if (!this.data){
                throw 'Expected data to be loaded';
            }
            
            return this.data ? this.data.id : null;
            
        };
        
        /**
         * Create an order for a dish
         * 
         * @param int  ID of the dish
         * @return object  Promise
         */
        Order.prototype.submitForDish = function(dishId){
            
            if (!dishId) {
                throw "Expected dish ID to be provided";
            }
                       
            var handler = (function(response){
                return response;
            }).bind(this);
            
            return TakeInAPI.post('/dish/' + dishId + '/order', {order: this.data}).then(handler);
            
        };
        
        /**
         * Load a single order from API
         * Must have set the ID in this.data.id, or
         * provide as an argument
         * 
         * @return Promise
         */
        Order.prototype.load = function(orderId){
            
            var handler = (function(response){
                if (response.order){
                    this.lastRetrieved = new Date();
                    this.data = response.order;
                    this.shadow = angular.extend({}, response.order);
                    return response.order;
                }
            }).bind(this);
            
            orderId = orderId || this.id();
            if (!orderId){
                throw 'Require an order ID to be set or provided as argument';
            }
            
            return TakeInAPI.get('/order/' + orderId).then(handler);
            
        };

        
        /**
         * Approve an order
         * Note: Only the seller is authorised to approve an order
         * 
         * @return object  Promise
         */
        Order.prototype.approve = function(){
            
            var handler = updatedResponseHandler.bind(this);
            return TakeInAPI.patch('/order/' + this.id(), {order: { accept_state: 'A' } }).then(handler);
            
        };
        
        /**
         * Reject an order
         * Note: Only the seller is authorised to reject an order
         * 
         * @return object  Promise
         */
        Order.prototype.reject = function(){
            
            var handler = updatedResponseHandler.bind(this);
            return TakeInAPI.patch('/order/' + this.id(), {order: { accept_state: 'R' } }).then(handler);
            
        };
       
        
        /**
         * Set a custom status on an order
         * Note: Only the seller is authorised to set a custom status on an order, a
         * the status code must be a single character
         * 
         * @return object  Promise
         */
        Order.prototype.customStatus = function(statusChar){
            
            statusChar = statusChar.substr(0,1);
            
            var handler = updatedResponseHandler.bind(this);
            return TakeInAPI.patch('/order/' + this.id(), {order: { accept_state: statusChar } }).then(handler);
            
        };
       
        /**
         * Reverse an order
         * Note: Only the seller is authorised to reverse an order
         * 
         * @return Promise
         */
        Order.prototype.reverse = function(){

            var handler = (function(response){
                if (response.success){
                    delete this.data;                           // Nuke it.. best you can do is delete all the data
                }
                else if (response.error){
                    console.warn(response.error);           // Remove this for production code
                }
                return response;
            }).bind(this);
            
            return TakeInAPI.delete('/order/' + this.id()).then(handler);
            
        };
        
        
        function updatedResponseHandler(response){
            this.lastSaved = new Date();
            return response;           
        }


        return Order;
        
    }]);
    
    
    //================
    // 2. OrderCollection Factory definition
    
    tiaResource.factory('OrderCollection', ['$http', '$q', 'TakeInAPI', 'Order', function($http, $q, TakeInAPI, Order){
        
        function OrderCollection(ordersData, cursor){
            //console.log("Created Order");
            this.orders = [];
            this.cursor = cursor;

            this.lastRetrieved = null;
            
            if (ordersData){
                ordersData.forEach(function(orderData){
                    this.push(new Order(orderData));
                }, this.orders);
            }
            
        }
        
        /**
         * Load orders for current requester
         * 
         * @return Promise
         */
        OrderCollection.prototype.loadForUser = function () {
            
            var handler = processLoadResults.bind(this);
            return TakeInAPI.get('/user/me/orders').then(handler);
            
        };

        /**
  * Load orders for current requester (as seller)
  * 
  * @return Promise
  */
        OrderCollection.prototype.loadForSeller = function () {

            var handler = processLoadResults.bind(this);
            return TakeInAPI.get('/user/me/orders?=seller').then(handler);

        };


        /**
* Load orders for current requester (as buyer)
* 
* @return Promise
*/
        OrderCollection.prototype.loadForBuyer = function () {

            var handler = processLoadResults.bind(this);
            return TakeInAPI.get('/user/me/orders?=buyer').then(handler);

        };

        
        /**
         * Load orders for a dish
         * The level of data returned will depend on whether the
         * requester is the dish owner, or anonymous
         * 
         * @return Promise
         */
        OrderCollection.prototype.loadForDish = function(dishId){
            
            var handler = processLoadResults.bind(this);
            return TakeInAPI.get('/dish/' + dishId + '/orders').then(handler);
            
        };
        
        
        //=================
        // Function definitions
        
        function processLoadResults(response){
            if (response.orders instanceof Array){
                this.lastRetrieved = new Date();
                this.orders = this.orders || [];
                response.orders.forEach(function(orderData){
                    this.push(new Order(orderData));
                }, this.orders);
                return response.orders;
            }
        }
        
        
        return OrderCollection;
        
    }]);
    
    
    
    
    

    
}());
