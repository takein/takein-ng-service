'use strict';
(function(){
    
    var tiaResource = angular.module('TakeInResource') || angular.module('TakeInResource', []);
    
    
    //======================
    // User Factory definition
    // Todo as of 2016-01-26
    // Will have methods such as User.getAll, User.get, 
    // User.reactivate, User.update, User.delete, etc.
    
    tiaResource.factory('User', ['$http', '$q', 'TakeInAPI', function($http, $q, TakeInAPI){
        
        function User(userData){        // expect to be an object
            
            this.data = {};              // User main record
            this.shadow = {};         // Shadow record useful to compare for changes
            this.image;                 // object containing image data (id & url)
            this.reviews = {};
            this.active = true;         // assume active by default
            this.passwordResetPending = function(){
                return !!this.data.force_password_set;
            };
            
            // Associated resource: Chef
            this.chef = new Chef();
            
            if (userData){
                this.chef = new Chef(userData.chef);
                this.data = userData;
                this.shadow = angular.extend({}, userData);
                this.bindImage();
                this.bindReviews();
                delete userData.chef;
            }
            
        }
        
        function Chef(chefData){
            
            this.data = {};
            this.shadow = {};
            
            if (chefData){
                this.data = chefData;
                this.data = angular.extend({}, chefData);
            }
            
        }
        
        
        /**
         * ID retrieval helper
         * 
         * @return ID
         */        
        User.prototype.id = function(){
            
            if (!this.data){
                throw 'Expected data to be loaded';
            }
            return this.data ? this.data.id : null;
            
        };
        
        
        /**
         * Load User from API
         * Will automatically bind the image
         * 
         * @param userId  ID of the user
         * @return null
         */
        User.prototype.load = function(userId, opts){
            
            opts = opts || {};
            
            if (!userId){
                throw "Expected userId to be provided";
            }
            
            var handler = (function(response){
                if (response.user){
                    this.data = response.user;
                    this.shadow = angular.extend({}, response.user);
                    this.bindReviews();

                    if (!opts.noImage){        // Bind images unless asked otherwise
                        this.bindImage();
                    }

                    return response.user;
                }
            }).bind(this);
            
            return TakeInAPI.get('/user/' + userId).then(handler);
            
        };
        
        
        /**
         * Gets image for this User
         * 
         * @return Object image resources
         */
        User.prototype.bindImage = function(){
            
            var handler = (function(response){
                if (response.image){
                    // bind the profile pic to the user object
                    this.image = response.image;
                    return response.image;
                }
            }).bind(this);
            
            var imageEndpoint = ['', 'user', this.id(), 'image'].join('/');
            return TakeInAPI.get(imageEndpoint).then(handler);
            
        };

        /**
        * Gets Reviews for this User
        * 
        * @return Object reviews resources
        */
        User.prototype.bindReviews = function () {
            //console.log("Running bindReviews");
            var handler = (function (response) {
                if (response.reviews) {
                    //console.log("Review Found:", response.reviews);
                    this.reviews = response.reviews;
                   // return response.reviews;
                } else {

                    //console.log("No reviews found");
                }
            }).bind(this);

            var reviewEndpoint = ['', 'user', this.id(), 'reviews'].join('/');
            return TakeInAPI.get(reviewEndpoint).then(handler);

        };


        
        /**
         * Updates data for user
         * 
         * @return Promise
         */
        User.prototype.update = function(){
            
            // Perform a delta, see what changed; add this to a delta array
            var determineDeltas = function(){
                var deltas = {};
                Object.keys(this.data).forEach(function(key){
                    // Generally objects wouldn't be updateable
                    if (typeof this.data[key] === 'object') return;
                    // Check if not equal to shadow
                    if (this.data[key] !== this.shadow[key]){
                        userDeltas[key] = this.data[key];
                    }
                }, this);
            };
            
            var handler = (function(response){
                
                if (response.success){
                    this.shadow = angular.extend({}, this.data);        // Update the shadow
                }
                else if (response.error){
                    this.data = angular.extend({}, this.shadow);        // Unshadow it    
                    console.warn(response.error);           // Remove console.warn for production code
                }
                return response;
                
            }).bind(this);
            
            return TakeInAPI.patch('/user/me', { 
                user: determineDeltas.bind(this), 
                chef: determineDeltas.bind(this.chef)
            }).then(handler);

        };
        
        /**
         * Updates password for a user
         * Search for new password in the following places:
         * + as the first argument to this function
         * + `this.password`
         * + `this.newPassword`
         * + `this.new_password`
         * + `this.data.password`
         * + `this.data.newPassword`
         * 
         * @return Promise
         */
        User.prototype.updatePassword = function(newPassword){
            
            // Resolve the new password
            newPassword = newPassword || (function(){
                var possibilities = [this.password, this.newPassword, this.new_password, this.data.password, this.data.newPassword, this.data.new_password];
                for (var x in possibilities){
                    if (possibilitiies.hasOwnProperty(x) && possibilities[x]){
                        return possibilities[x];
                    }
                }
                
            }.bind(this));
            
            var handler = (function(response){
                
                if (response.success){
                    this.shadow = angular.extend({}, this.data);
                }
                else if (response.error){
                    this.data = angular.extend({}, this.shadow);
                    console.warn(response.error);
                }
                return response;
                
            }).bind(this);
            
            return TakeInAPI.patch('/user/me', {
                password: newPassword
            }).then(handler);
            
        };
        
        
        
        /**
         * Deactivate a user
         * 
         * @return Promise
         */
        User.prototype.remove = function(){

            var handler = (function(response){
                if (response.success){
                    this.active = false;        // successfully deactivated    
                }
                else if (response.error){
                    console.warn(response.error);           // Remove this for production code
                }
                return response;
            }).bind(this);
            
            return TakeInAPI.delete('/user/' + this.id()).then(handler);
            
        }; 
        
        
        return User;
    }]);
    
    
    
    //======================
    // 2. User search results Factory definition
    
    tiaResource.factory('UserSearchResults', ['$http', '$q', 'TakeInAPI', 'Dish', function($http, $q, TakeInAPI, Dish){
        
        function UserSearchResults(usersData){
            
            this.users = [];

            if (usersData){
                usersData.forEach(function(userData){
                    this.push(new User(userData));
                }, this.users);
            }
            
        }
        
        UserSearchResults.prototype.load = function(){
            
            var handler = (function(response){
                this.cursor = response.cursor;
                if (response.users instanceof Array){
                    this.users = this.users || [];
                    response.users.forEach(function(userData){
                        this.push(new User(userData));
                    }, this.users);
                    return response.users;
                }
            }).bind(this);
            
            return TakeInAPI.getAll('/users').then(handler);
            
        };
        
        return UserSearchResults;
        
    }]);
    
    
    
    
    
}());