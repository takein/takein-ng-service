'use strict';
(function(){
    
    var tiaResource = angular.module('TakeInResource') || angular.module('TakeInResource', []);
    
    
    //======================
    // Auth Factory definition
    // Todo as of 2016-01-26
    // Will have methods such as Auth.signUp, Auth.login, Auth.logout, Auth.verify, 
    // Auth.currentProfile, Auth.forgotPassword, Auth.activateCode, .... etc
    
    tiaResource.factory('Auth', ['$q', 'TakeInAPI', function($q, TakeInAPI){
        
        function Auth(){
            
            this.status = null;
            
        }

        /**
         * API Token retrieval helper
         * 
         * @return ID
         */        
        Auth.prototype.apiToken = function(){
            return TakeInAPI.configured().apiToken;
        };
        
        
        /**
         * Perform a login
         * 
         * @param string username  Username or Email ID
         * @param string password  Plain text password
         * @param Object opts  Any extra options
         * @return Promise
         */
        Auth.prototype.login = function(username, password, opts){

            opts = opts || {};
            
            if (!username || !password){
                throw "Expect username and password as args[1-2]";
            }
            
            var handler = (function(response){
                if (response.disabled){
                    console.warn('The account is disabled or unactivated');
                }
                if (response.needs_reset){
                    console.warn('The password requires setting');
                }
                return response;
            }).bind(this);
            
            return TakeInAPI.basic('/login', username, password).then(handler);

         };
         
                 
        /**
         * Perform a logout
         * 
         * @param Object opts  Any extra options
         * @return Promise
         */
        Auth.prototype.logout = function(opts){

            opts = opts || {};
            
            var handler = (function(response){
                return response;
            }).bind(this);
            
            return TakeInAPI.post('/logout', []).then(handler);

         };
         
         
        /**
         * Get requester's authenticated status. By side effect, it also
         * grabs your user information, if you are in fact authenticated.
         * 
         * @param Object opts  Any extra options
         * @return Promise
         */
        Auth.prototype.loadStatus = function(opts){

            opts = opts || {};
            
            var handler = (function(response){
                this.status = !!response.error;
                return response;
            }).bind(this);
            
            return TakeInAPI.get('/user/me').then(handler);

        };
        
        
        return Auth;
        
    }]);
    
}());
