'use strict';
(function(){
    
    // var tiaResource = angular.module('TakeInResource', ['ngResource']);
    var tiaResource = angular.module('TakeInResource') || angular.module('TakeInResource', []);
    
    
    //================
    // Contains:
    // 1. Dish factory
    // 2. DishCollection factory
    //================
    
    
    //================
    // 1. Dish Factory definition
    // Todo: 
    // photo uploading? (or is this nice-to-have)
    // create reviews straight from dish
    
    tiaResource.factory('Dish', ['$http', '$q', 'TakeInAPI', function($http, $q, TakeInAPI){
        
        function Dish(dishData){
            
            this.data = {};              // Dish main record
            this.shadow = {};         // Shadow record useful to compare for changes
            this.images = [];           // images to be loaded later
            
            this.lastRetrieved = null;
            this.lastSaved = null;
            
            if (dishData){
                this.data = dishData;
                this.shadow = angular.extend({}, dishData);
                this.bindImages();
            }
            
        }

        /**
         * ID retrieval helper
         * 
         * @return mixed  ID
         */        
        Dish.prototype.id = function(){
            
            if (!this.data){
                throw 'Expected data to be loaded';
            }
            
            return this.data ? this.data.id : null;
            
        };
        
        /**
         * Load dish from API
         * Will automatically bind the image
         * 
         * @param string  ID of the dish
         * @return object  Promise
         */
        Dish.prototype.load = function(dishId, opts){
            
            opts = opts || {};
            
            if (!dishId){
                throw "Expected dishId to be provided";
            }
            
            var handler = (function(response){
                if (response.dish){
                    this.lastRetrieved = new Date();
                    this.data = response.dish;
                    this.shadow = angular.extend({}, response.dish);
                    if (!opts.noImages){        // Bind images unless asked otherwise
                        this.bindImages();
                    }
                    return response.dish;
                }
            }).bind(this);
            
            return TakeInAPI.get('/dish/' + dishId).then(handler);
            
        };
        
        /**
         * Gets image for this dish
         * 
         * @return Object[] image resources
         */
        Dish.prototype.bindImages = function(){
            
            var handler = (function(response){
                if (response.images){
                    // bind the images to the dish object
                    this.images = response.images;
                    return response.images;
                }
            }).bind(this);
            
            return TakeInAPI.get('/dish/' + this.id() + '/images').then(handler);
            
        };
                
        /**
         * Updates data for dish
         * 
         * @return Promise
         */
        Dish.prototype.update = function(){
            
            // Perform a delta, see what changed; add this to a delta array
            var deltas = {};
            Object.keys(this.data).forEach(function(key){
                // Generally objects wouldn't be updateable
                if (typeof this.data[key] === 'object') return;
                // Check if not equal to shadow
                if (this.data[key] !== this.shadow[key]){
                    deltas[key] = this.data[key];
                }
            }, this);
            
            var handler = (function(response){
                
                if (response.success){
                    // Update the shadow
                    this.lastSaved = new Date();
                    this.shadow = angular.extend({}, this.data);
                }
                else if (response.error){
                    // Unshadow it
                    this.data = angular.extend({}, this.shadow);
                    console.warn(response.error);           // Remove console.warn for production code
                }
                
                return response;
                
            }).bind(this);
            
            return TakeInAPI.patch('/dish/' + this.id(), {dish: deltas}).then(handler);
            
        };

        /**
         * Delete a dish
         * 
         * @return Promise
         */
        Dish.prototype.remove = function(){

            var handler = (function(response){
                if (response.success){
                    delete this.data;                           // Nuke it.. best you can do is delete all the data
                }
                else if (response.error){
                    console.warn(response.error);           // Remove this for production code
                }
                return response;
            }).bind(this);
            
            return TakeInAPI.delete('/dish/' + this.id()).then(handler);
            
        };
        
        /**
         * Create a dish 
         * 
         * @return Promise
         */
        Dish.prototype.create = function(){
            
            var handler = (function(response){
                this.lastSaved = new Date();
                this.data.id = response.success.id;         // Bind the new ID to this object
                this.shadow = angular.extend({}, this.data);
                return response;
            }).bind(this);
            
            return TakeInAPI.post('/dish', {dish: this.data}).then(handler);
            
        };

        return Dish;
    }]);
    
    
    //======================
    // 2. DishCollection Factory definition
    
    tiaResource.factory('DishCollection', ['$http', '$q', 'TakeInAPI', 'Dish', function($http, $q, TakeInAPI, Dish){
        
        function DishCollection(dishesData, cursor){
            
            this.dishes = [];
            this.cursor = cursor;

            this.lastRetrieved = null;
            
            if (dishesData){
                dishesData.forEach(function(dishData){
                    this.push(new Dish(dishData));
                }, this.dishes);
            }
            
        }
        
        /**
         * Load dishes data from TakeInAPI
         * 
         * @param max  Maximum number of results to load
         * @return null
         */
        DishCollection.prototype.load = function(max, cursorDirection){
            
            var handler = (function(response){
                this.cursor = response.cursor;
                if (response.dishes instanceof Array){
                    this.lastRetrieved = new Date();
                    this.dishes = this.dishes || [];
                    response.dishes.forEach(function(dishData){
                        this.push(new Dish(dishData));
                    }, this.dishes);
                    return response.dishes;
                }
            }).bind(this);
            
            // Build the query-string args (qsa)
            var qsa = { max: max || 20 };
            if (cursorDirection && this.cursor){       // A non-falsy cursorDirection tells us to choose cursor.after or cursor.before
                if (cursorDirection > 0){
                    qsa = angular.extend(qsa, {after: this.cursor.cursorAfter});
                }
                else {
                    qsa = angular.extend(qsa, {before: this.cursor.cursorBefore});
                }
            }
            
            return TakeInAPI.getAll('/dishes', {queryString: qsa}).then(handler);
            
        }
        
        /**
         * Gets images for all dishes within the collection
         * Assuming none of the dishes have their individual images
         * 
         * @return Promise
         */
        DishCollection.prototype.bindAllImages = function(){
            
            var dishes = this.dishes;
            var deferred = $q.defer();
            var imagePromises = [];
            dishes.forEach(function(dish){
                imagePromises.push(TakeInAPI.get('/dish/'+dish.id+'/images'));      // returns a thenable. Might not work...
            });
            
            // Get them all
            $q.all(imagePromises).then(function(results){
                // Bind results to each dish
                results.forEach(function(response, index){
                    dishes[index].images = response.images;
                });
                deferred.resolve();
            }).catch(function(errors){
                deferred.reject(errors);
            });
            
            return deferred.promise;
            
        }; 
        
        /**
         * Load more items into this collection. 
         * Returns items that were created chronologically later
         * than the current page
         * 
         * @return Promise
         */
        DishCollection.prototype.loadAfter = function(){
            console.log('loadAfter');
        };
        /**
         * Load more items into this collection. 
         * Returns items that were created chronologically before
         * than the current page
         * 
         * @return Promise
         */
        DishCollection.prototype.loadBefore = function(){
            console.log('loadBefore');
        };
        
        return DishCollection;
    }]);
    
}());