'use strict';
(function(){
   // Use existing test harness for TIA Resource Service
    var app = angular.module('testHarnessApp');

    app.controller('authTestHarness', ['$scope', '$rootScope', 'Auth', 'User', function($scope, $rootScope, Auth, User){

        $scope.cred = {};
        
        var auth = new Auth();
        $scope.auth = auth;
        $scope.lastApiToken = '(not retreived)';
    
        // Login
        $scope.submitLogin = function(){
            console.log('submitting');
            auth.login($scope.cred.username, $scope.cred.password).then(function(resp){
                $scope.lastApiToken = resp.api_token;
                $rootScope.$broadcast('loginEvent');
            });
        };
        
        // Logout
        $scope.submitLogout = function(){
            auth.logout().then(function(){
               $rootScope.$broadcast('loginEvent');
            });
        };
        

    }]); 
    
}());