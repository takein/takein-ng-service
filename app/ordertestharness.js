'use strict';
(function(){
   // Use existing test harness for TIA Resource Service
    var app = angular.module('testHarnessApp');

    app.controller('orderTestHarness', ['$scope', '$rootScope', 'Order', 'Dish', 'User', function($scope, $rootScope, Order, Dish, User){

        $scope.orderForm = {};      // Stores the order form data
        $scope.orderDishId = null;
        
        var orderDish = new Dish();
        $scope.orderDish = orderDish;       // expose to the scope
        
        
        //========================
        // How to lookup existing orders
        //========================       
        
        $scope.existingOrders = []
        $scope.retrieveExistingOrders = function(){
           var OrderCollection 
            
        };
        
        
        //========================
        // How to submit brand new orders
        //========================
        
        $scope.submitOrder = function(){
            
            // Create a new instance
            // You can fill it up with data via the constructor
            var newOrder = new Order($scope.orderForm);
            
            // Or you could create the object first, then slowly fill in the data, e.g.
            // var newOrder = new Order();
            // newOrder.data.servings_purchased = 2;
            // newOrder.data.pickup_delivery = 'P';
            
            // Then submit the order to the correct Dish ID
            newOrder.submitForDish($scope.orderDishId).then(function(resp){
                
                // Expect success
                if (resp.success){
                    var newOrderId = resp.success.id;
                    
                    // You could now navigate to a specific order screen, or
                    // take the user back to the newsfeed
                    
                }
                
                
            }).catch(function(resp){
               
                // Expect any errors
                console.warn(resp);
               
            });
            
        };
        
        
        
        
        //=====================
        // Watch
        // When the user types in their dish, 
        // we can auto-lookup the info
        
        $scope.$watch('orderDishId', function(newDishId){
            if (newDishId){
                orderDish.load(newDishId);
            }
        });
        
        
    }]); 
    
}());