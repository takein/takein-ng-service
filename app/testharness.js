'use strict';
(function(){
    // Test harness for TIA Resource Service
    var app = angular.module('testHarnessApp', ['TakeInResource']);

    app.config(['TakeInAPIProvider', function(TakeInAPIProvider){
        
        // Set the Base URL e.g. http://tia-dev.alanyeung.net/api/v1
        // according to the environment. Must omit trailing slash.
        TakeInAPIProvider.setBaseUrl('https://dev.take-in.net.au/api/v1');
        
        // Check if localstorage has an Api Token
        if (window.localStorage && window.localStorage.getItem('takeInApiToken')){
            TakeInAPIProvider.setApiToken(window.localStorage.getItem('takeInApiToken'));
        }
        else {
            // Or else, prompt for the API Token
            var inputApiToken = prompt('Enter the API Token, or cancel for none');
            if (inputApiToken){
                window.localStorage.setItem('takeInApiToken', inputApiToken);
                TakeInAPIProvider.setApiToken(inputApiToken);
            }        
        }
        
        //$2y$10$AhteuNZ85yVgh8iBTek6UOqANNLPlspsqIh/8atHCyMZ2OoCvGQCW   :: Anna
        
    }]);

    app.controller('testHarness', ['$scope', 'User', function($scope, User){

        // A temp dumping ground. Don't use in production.
        $scope.opts = {
            fetchHowMany: 1,
            fetchDishId: 1,
            newDish: {},
        }; 
        
        $scope.screen = 'start';
        
        $scope.resetApiToken = function(){
            window.localStorage.clear();
            window.location.reload();
        };
        
        // Fetch who you are
        $scope.user = new User();
        $scope.user.load('me');
        
        
        // Scope state variables -- useful.
        $scope.isLoading = false;       // This is one way to show your loading spinner until all the data loads
        $scope.isSending = false;       // This is one way to show your sending note until the call finishes

        
        //==================================
        // Optional:
        // loadingRequest() handles the isLoading() and 
        // other display-related things, allowing the getter
        // etc methods to focus on business logic rather than
        // loading spinners. 
        // These are just bound to the scope for convenience, but 
        // really should be abstracted into a provider or something
        
        $scope.handleLoadingRequest = handleLoadingRequest;
        $scope.handleSendingRequest = handleSendingRequest;
        
        
        //=====================
        // Function definitions
        
        function handleLoadingRequest(promise, handler){
            
            handler = handler || function(){};
            this.isLoading = true;
            
            promise
                .then(function(promiseResponse){
                    handler(promiseResponse);
                })
                .catch(function(promiseResponse){
                    console.warn(promiseResponse);
                    handler(promiseResponse);
                })
                .finally(function(){
                    this.isLoading = false;
                });
                
        }
        
        function handleSendingRequest(promise, handler){
            
            handler = handler || function(){};
            this.isSending = true;
            
            promise
                .then(function(promiseResponse){
                    handler(promiseResponse);
                })
                .catch(function(promiseResponse){
                    console.warn(promiseResponse);
                    handler(promiseResponse);
                })
                .finally(function(){
                    this.isSending = false;
                });
                
        }
        
        //=====================
        // Listeners:
        // This is just for the purposes of this test harness
        // to keep the current user refreshed everywhere
        
        // Do some when logins happen
        $scope.$on('loginEvent', function(){
            $scope.user.load('me');
        });
        
        
        
           
    }]);

    // Warning, don't do this in production. 
    // We are creating a controller JUST to expose the API token, which is not such a good idea.
    app.controller('testHarnessMeta', ['$scope', 'TakeInAPI', 'User', function($scope, TakeInAPI, User){
        
        $scope.configs = {};
        $scope.user = new User();
        reloadApiConfigs.call($scope);
        
        // Listen for loginevent being broadcast
        $scope.$on('loginEvent', function(){
            window.localStorage.setItem('takeInApiToken', TakeInAPI.configured().apiToken);
            reloadApiConfigs.call($scope);
        });
        
        function reloadApiConfigs(){
            // Note it returns a read only copy
            this.configs = TakeInAPI.configured();
            this.user.load('me');       // trigger a reload
        };
    }]);  
}());