'use strict';
(function(){
   // Use existing test harness for TIA Resource Service
    var app = angular.module('testHarnessApp');

    app.controller('userTestHarness', ['$scope', '$rootScope', 'User', function($scope, $rootScope, User){

        $scope.lookupUserId = null;
        $scope.lookupUser = new User();
        
        $scope.$watch('lookupUserId', function(newUserId){
            if (newUserId){
                $scope.lookupUser.load(newUserId);
            }
        });
    
        
        //========================
        // Updating the user
        //========================
        
        $scope.updateSelf = function(){
            
            // Create a new instance
            // You can fill it up with data via the constructor
            var newOrder = new Order($scope.orderForm);
            
            // Or you could create the object first, then slowly fill in the data, e.g.
            // var newOrder = new Order();
            // newOrder.data.servings_purchased = 2;
            // newOrder.data.pickup_delivery = 'P';
            
            // Then submit the order to the correct Dish ID
            newOrder.forDish($scope.orderDishId).then(function(resp){
                
                // Expect success
                if (resp.success){
                    var newOrderId = resp.success.id;
                    
                    // You could now navigate to a specific order screen, or
                    // take the user back to the newsfeed
                    
                }
                
                
            }).catch(function(resp){
               
                // Expect any errors
                console.warn(resp);
               
            });
            
            
        };

    }]); 
    
}());