'use strict';
(function(){
   // Use existing test harness for TIA Resource Service
    var app = angular.module('testHarnessApp');

    app.controller('dishTestHarness', ['$scope', 'DishCollection', 'Dish', 'User', function($scope, DishCollection, Dish, User){

        // Create the ResourceCollection object
        var dishCollection = new DishCollection();
        
        // For kicking off an "get all" request
        $scope.dishGetAll = function(max, cursorDirection){
            
            // Bind the DishCollection's dishes array to the $scope
            $scope.dishes = dishCollection.dishes; 
            
            // Load it. When you do, the $scope.dishes binding will 
            // automatically update as soon as data comes in.
            $scope.handleLoadingRequest.call($scope, dishCollection.load(max, cursorDirection), function(dishes){
                
                // Do something else with dishes if you want
                // The dish info would already be automatically loaded 
                // into $scope.dishes, as would the images.
                
            });
        };
        
        
        // For kicking off a "get single" request
        $scope.dishGet = function(dishId){
            
            // Note:
            // A pure request (minus the wrappedRequest) would be:
            // TakeInAPI.dish(dishId).then(function(){ ... });
            
            var dish = new Dish();
            $scope.dishes = [dish];
            
            $scope.handleLoadingRequest.call($scope, dish.load(dishId), function(dish){
                
                // Do something else with this dish if you want
                
            });
        };
        
        
        // For kicking off a create request
        // Note: The below is just an arbitrary name
        $scope.dishCreate = function(){
            
            var dish = new Dish($scope.opts.newDish);
            
            $scope.handleSendingRequest.call($scope, dish.create(), function(result){
                
                if (result.success){
                    alert('Your dish has been saved');
                }
                else {
                    alert('Uh-oh, your dish wasnt saved');
                }
                
            });
        };
           
    }]); 
    
}());